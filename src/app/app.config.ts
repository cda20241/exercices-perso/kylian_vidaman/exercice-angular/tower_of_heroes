import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { HeroeService } from './services/heroe.service';

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes), HeroeService]
};
