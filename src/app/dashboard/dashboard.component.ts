import { NgFor } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Subscription } from 'rxjs';
import { HeroeComponent } from '../heroe/heroe.component';
import { HeroeService } from '../services/heroe.service';

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [NgFor, HeroeComponent, RouterLink],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss',
})
export class DashboardComponent implements OnInit {
  heroes!: any[];
  heroeSubscription!: Subscription;

  constructor(private heroeService: HeroeService) {}

  ngOnInit(): void {
      this.heroeSubscription = this.heroeService.heroeSubject.subscribe(
        (heroes: any[]) => {
          this.heroes = heroes.slice(0,4);
        }
      );
      this.heroeService.emitHeroeSubject();
  }
}
