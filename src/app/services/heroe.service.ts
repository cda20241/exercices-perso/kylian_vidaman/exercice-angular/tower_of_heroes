import { Subject } from 'rxjs';

export class HeroeService {
  heroeSubject = new Subject<any[]>();

  private heroes = [
    {
      id: 1,
      name: 'John',
    },
    {
      id: 2,
      name: 'Jane',
    },
    {
      id: 3,
      name: 'Michael',
    },
    {
      id: 4,
      name: 'Emily',
    },
    {
      id: 5,
      name: 'David',
    },
    {
      id: 6,
      name: 'Sarah',
    },
    {
      id: 7,
      name: 'Daniel',
    },
    {
      id: 8,
      name: 'Olivia',
    },
    {
      id: 9,
      name: 'Matthew',
    },
    {
      id: 10,
      name: 'Sophia',
    },
  ];

  emitHeroeSubject() {
    this.heroeSubject.next(this.heroes.slice());
  }

  getHeroeById(id: number){
    return this.heroes.find((e) => e.id === id);
  }

  addHeroe(name: string) {
    const heroeObject = {
        id: 0,
        name: ''
    };

    heroeObject.name = name;
    heroeObject.id = this.heroes[this.heroes.length - 1].id + 1;
    this.heroes.push(heroeObject);
    this.emitHeroeSubject();
  }

}
