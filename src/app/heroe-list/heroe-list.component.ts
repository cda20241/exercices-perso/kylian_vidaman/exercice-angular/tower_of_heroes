import { NgFor } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Subscription } from 'rxjs';
import { Heroe } from '../models/Heroes.model';
import { HeroeService } from '../services/heroe.service';

@Component({
  selector: 'app-heroe-list',
  standalone: true,
  imports: [NgFor, RouterLink],
  templateUrl: './heroe-list.component.html',
  styleUrl: './heroe-list.component.scss'
})
export class HeroeListComponent implements OnInit{

  heroes: Heroe[] = [];
  heroeSubscription: Subscription = new Subscription;

  constructor(private heroeService: HeroeService){}

  ngOnInit(): void {
      this.heroeSubscription = this.heroeService.heroeSubject.subscribe(
        (heroes : Heroe[]) => {
          this.heroes = heroes;
        }
      );
      this.heroeService.emitHeroeSubject();
  }

  ngOnDestroy() {
    this.heroeSubscription.unsubscribe();
  }

}
