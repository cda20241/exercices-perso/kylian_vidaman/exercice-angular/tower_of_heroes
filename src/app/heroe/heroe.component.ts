import { Component, Input, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { HeroeService } from '../services/heroe.service';

@Component({
  selector: 'app-heroe',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './heroe.component.html',
  styleUrl: './heroe.component.scss',
})
export class HeroeComponent implements OnInit{
  @Input()
  heroName!: string;
  @Input()
  indexOfHero!: number;
  @Input()
  heroId!: number;

  constructor(private heroService: HeroeService) {}

  ngOnInit() {}
}
