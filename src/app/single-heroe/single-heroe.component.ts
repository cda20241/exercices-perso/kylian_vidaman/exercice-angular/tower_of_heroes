import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HeroeService } from '../services/heroe.service';

@Component({
  selector: 'app-single-heroe',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './single-heroe.component.html',
  styleUrl: './single-heroe.component.scss',
})
export class SingleHeroeComponent {
  id!: number;
  name!: string;

  constructor(
    private heroeService: HeroeService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    const id = +this.route.snapshot.params['id'];
    const heroe = this.heroeService.getHeroeById(id);
    if (heroe) {
      this.id = heroe.id;
      this.name = heroe.name;
    }
  }

  goBack(form: NgForm): void {
    this.location.back();
    const newName = form.value['name']
    const id = +this.route.snapshot.params['id'];
    const heroe = this.heroeService.getHeroeById(id);
    if(heroe) {
      heroe.name = newName
    }
  }

}
