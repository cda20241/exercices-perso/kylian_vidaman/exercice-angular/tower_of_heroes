import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleHeroeComponent } from './single-heroe.component';

describe('SingleHeroeComponent', () => {
  let component: SingleHeroeComponent;
  let fixture: ComponentFixture<SingleHeroeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SingleHeroeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SingleHeroeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
