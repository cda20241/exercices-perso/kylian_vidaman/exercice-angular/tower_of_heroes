import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FourOfFourComponent } from './four-of-four/four-of-four.component';
import { HeroeListComponent } from './heroe-list/heroe-list.component';
import { SingleHeroeComponent } from './single-heroe/single-heroe.component';

export const routes: Routes = [
  { path: 'heroe', component: HeroeListComponent },
  { path: 'heroe/:id', component: SingleHeroeComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: '', component: DashboardComponent },
  { path: '404', component: FourOfFourComponent},

];
